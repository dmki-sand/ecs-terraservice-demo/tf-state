.PHONY: clean
clean:
	rm -rf .terraform  terraform.tfstate* errored.tfstate

.PHONY: init
init:
	# This is a new environment without statefile, so create it and
	# add this terraservice as its first tennant
	terraform init ;\
	if [ ! -e ./terraform.tf ]; then \
		terraform apply -var-file=eu-west-1.tfvars -auto-approve ;\
		terraform init  -force-copy ;\
		git add terraform.tf ;\
		git commit -m "PIPELINE/AUTO/update backend config" ;\
		git push origin master ;\
	fi

.PHONY: apply
apply: init
	# Terraform state file esists so treat as ordinary terraservice
	terraform apply -var-file=eu-west-1.tfvars -auto-approve

.PHONY: destroy
destroy:
	# This is tricky as when we destroy the resources, we destroys
	# the terraform state file we need to update, really in a prod
	# environment we would simply not do this as deleteing state
	# automajically is just too painful should it go wrong.
	terraform state pull > ./terraform.tfstate ;\
	git rm terraform.tf ;\
	rm terraform.tf ;\
	terraform init  ;\
	terraform destroy -var-file=eu-west-1.tfvars -auto-approve ;\
	git commit -m "PIPELINE/AUTO/remove backend config" ;\
	git push origin master
