variable "region" {}
variable "role_arn" { default = "" }
variable "tags" {}

variable "deploy_acc_name" {}
variable "force_destroy" {
  type        = string
  default     = "true"
  description = "Should terraform destroy succeed with s3 statck bucket destruction?"
}
