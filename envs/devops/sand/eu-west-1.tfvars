deploy_acc_name = "newiotideas"
region          = "eu-west-1"
tags = {
  Cost_centre = "1234"
  Description = "Remote State for Terraform"
  Env         = "devops"
  Stage       = "sand"
  Product     = "tf-state"
  Repo        = "https://gitlab.com/Inmandmk/tf-state.git"
}
