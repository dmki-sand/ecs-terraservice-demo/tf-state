provider "aws" {
  region = var.region

  # assume_role {
  #   role_arn = var.role_arn
  # }
}

module "tf-init" {
  source          = "../../../modules"
  region          = var.region
  deploy_acc_name = var.deploy_acc_name
  force_destroy   = var.force_destroy
  tags            = var.tags
}

output "state_bucket_arn" {
  value = module.tf-init.state_bucket_arn
}
