terraform {
  required_version = ">= 0.12.2"

  backend "s3" {
    region         = "eu-west-1"
    bucket         = "newiotideas-eu-west-1-remote-state"
    key            = "remote-state/terraform.tfstate"
    dynamodb_table = "newiotideas-eu-west-1-remote-state-lock"
    profile        = ""
    role_arn       = ""
    encrypt        = "true"
  }
}
