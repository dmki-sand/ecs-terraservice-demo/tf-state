deploy_acc_name = "dika"
region          = "eu-west-1"
tags = {
  Cost_centre = "1234"
  Description = "Remote State for Terraform"
  Env         = "dika"
  Stage       = "prod"
  Product     = "tf-remote-state"
  Repo        = "https://gitlab.com/Inmandmk/tf-remote-state.git"
}
