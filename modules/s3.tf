module "terraform_state_backend" {
  source                             = "cloudposse/tfstate-backend/aws"
  version                            = "0.9.0"
  region                             = var.region
  role_arn                           = var.role_arn
  terraform_state_file               = "remote-state/terraform.tfstate"
  terraform_backend_config_file_path = "."
  namespace                          = var.deploy_acc_name
  environment                        = var.region
  name                               = "remote"
  attributes                         = ["state"]
  force_destroy                      = var.force_destroy
  tags                               = var.tags
}

output "state_bucket_arn" {
  value = module.terraform_state_backend.s3_bucket_arn
}
