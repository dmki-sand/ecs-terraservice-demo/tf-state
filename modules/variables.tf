variable "region" {}
variable "role_arn" { default = "" }
variable "tags" {}

variable "deploy_acc_name" {
  type        = string
  description = "Name of the account resources being deployed to"
}

variable "force_destroy" {
  type        = string
  default     = "false"
  description = "Should terraform destroy succeed with s3 statck bucket destruction?"
}
